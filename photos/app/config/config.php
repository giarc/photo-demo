<?php
define("BASE_URL", 'http://127.0.0.1/~cwt/photos/');
define("BASE_PATH", "/Users/cwt/Code/arcanum/photos/public/media/");

return new \Phalcon\Config(array(
    'database' => array(
        'adapter'     => 'Mysql',
        'host'        => '127.0.0.1',
        'username'    => 'arcanum',
        'password'    => 'password',
        'dbname'      => 'arcanum',
    ),
    'application' => array(
        'controllersDir' => __DIR__ . '/../../app/controllers/',
        'modelsDir'      => __DIR__ . '/../../app/models/',
        'viewsDir'       => __DIR__ . '/../../app/views/',
        'pluginsDir'     => __DIR__ . '/../../app/plugins/',
        'libraryDir'     => __DIR__ . '/../../app/library/',
        'cacheDir'       => __DIR__ . '/../../app/cache/',
        'baseUri'        => BASE_URL,
    )
));
