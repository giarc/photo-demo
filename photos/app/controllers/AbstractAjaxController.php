<?php

abstract class AbstractAjaxController extends Phalcon\Mvc\Controller {
protected $_isJsonResponse = false;

    public function setJsonResponse() {
        $this->view->disable();

        $this->_isJsonResponse = true;
        $this->response->setContentType('application/json', 'UTF-8');
    }

    public function afterExecuteRoute(\Phalcon\Mvc\Dispatcher $dispatcher) {
        if ($this->_isJsonResponse) {
            $data = $dispatcher->getReturnedValue();
            if (is_array($data)) {
                $data = json_encode($data);
            }

             $this->response->setContent($data);
        }
        return $this->response->send();
    }
}