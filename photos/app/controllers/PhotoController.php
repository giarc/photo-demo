<?php

/*
 * Controller to handle POST and GET requests from angular js.
 */
class PhotoController extends AbstractAjaxController {

    /*
     * @returns json array of user data
     * @param $id: the id of the user/photographer you want to view.
     */
    public function viewAction($id){
        $this->setJsonResponse();

        $user = Users::findFirst("id='$id'");
        $base_url = BASE_URL;
        $media_url = "$base_url/media/{$user->getImageFile()}";
        $data = array();
        $data['fullname'] = $user->getFullname();
        $data['gender'] = $user->getGender();
        $data['statement'] = $user->getStatement();
        $data['image'] = $media_url;


        $interests = array();
        foreach($user->UsersInterests as $interest){
            $interests[] = $interest->PhotoInterests->getLabel();
        }
        $data['interests'] = $interests;

        return $data;
    }


    /*
     * @params POST data to create user database record from.
     *
     * Will attempt to resize submitted image.
     */
    public function indexAction(){
        $fname = $_FILES['file']['name'];

        $user = new Users();
        $user->setFullname($this->request->getPost("fullname"));
        $user->setGender($this->request->getPost("gender"));
        $user->setStatement($this->request->getPost("statement"));
        $user->setImageFile($this->request->getPost("image_file"));
        $user->setImageFile($fname);
        $user->save();

        $path = BASE_PATH;
        $filename = "$path/$fname";
        move_uploaded_file( $_FILES['file']['tmp_name'], $filename);

        $error = $this->resize_image($filename);
        if($error){
            $user->setImageFile($error);
            $user->save();
        }

        foreach (json_decode($this->request->getPost("interests")) as $k => $v) {
            $interest = new UsersInterests();
            $photoint = PhotoInterests::findFirst("label='$k'");
            $interest->setUserId($user->getId());
            $interest->setInterestId($photoint->getId());
            $interest->save();
        }
        $this->setJsonResponse();
        return array('user_id' => $user->getId());
    }


    /*
     * @params filename to attempt a resize on.
     * */
    private function resize_image($filename){
        try {
            $extension = strtolower(end(explode('.', $filename)));

            if ($extension == "jpg" || $extension == "jpeg") {
                $src = imagecreatefromjpeg($filename);
            } else if ($extension == "png") {
                $src = imagecreatefrompng($filename);
            }

            list($width, $height) = getimagesize($filename);

            $new_width = 800;
            $new_height = ($height / $width) * $new_width;

            $tmp = imagecreatetruecolor($new_width, $new_height);
            imagecopyresampled($tmp, $src, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
            imagejpeg($tmp, $filename, 100);

            imagedestroy($src);
            imagedestroy($tmp);
        }
        catch(Exception $e){
            return $e->getMessage();
        }

    }
}