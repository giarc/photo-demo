'use strict';

/* Controllers */

var photoControllers = angular.module('photoControllers', []);

/*
 *Make an HTTP POST request in order to upload image file
 * and user submission data.
 */
photoControllers.controller('photoCtrl', ['$scope', '$http', '$upload', '$location',
    function($scope, $http, $upload, $location) {
        $http.get('data/interests.json').success(function (choices) {
            $scope.interestChoices = choices;
        });

        $scope.progress = '';
        $scope.user = {};
        $scope.user.interests = {};

        $scope.somethingSelected = function (object) {
            return Object.keys(object).some(function (key) {
                return object[key];
            });
        }

        $scope.uploadFile = function($files){
            $scope.user.files = $files;
            $scope.$apply();
        };

        $scope.processForm = function(){

            $upload.upload({
               url: upload_url
                , data: $scope.user
                , file: $scope.user.files[0]
            }).progress(function(evt){
                var pct = 'percent: ' + parseInt(100.0 * evt.loaded / evt.total) + "%";
                $scope.progress = pct;
                console.log(pct);
            }).success(function(data){
                $scope.progress = "Done!"
                console.log(data.user_id);
                $location.path("/" + data.user_id);
            });
        };
    }]);

/*
 *Make an HTTP GET request for the user id, inject json data into the view.
 */
photoControllers.controller('photoDetailCtrl', ['$scope', '$http', '$routeParams',
    function($scope, $http, $routeParams){
        $http.get(view_url +$routeParams.userId).success(function(data) {
            $scope.photographer = data;
        });
    }
]);