'use strict';


var photoApp = angular.module('photoApp', [
  'ngRoute'
  , 'photoControllers'
  , 'angularFileUpload'
]);

var base_url = 'http://127.0.0.1/~cwt/photos/photo';
var upload_url = base_url;
var view_url = base_url + '/view/';

photoApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/', {
                templateUrl: 'partials/signup.html',
                controller: 'photoCtrl'
            }).
            when('/:userId', {
                templateUrl: 'partials/photographer.html',
                controller: 'photoDetailCtrl'
            }).
            otherwise({
               redirectTo: '/'
            });
    }]);

