'use strict';

/* Services */

var photoServices = angular.module('photoServices', ['ngResource']);
var phonecatServices = angular.module('phonecatServices', ['ngResource']);
photoServices.factory('Photo', ['$resource',
    function($resource) {
        return $resource('/:photoId', {}, {
           query: {method:'GET', params:{photoID:'photo'}, isArray:true}
        });
    }
]);
