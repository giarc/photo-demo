# README #

* Quick summary
Demo AngularJS submission.

* Version 0.01

* Dependencies
Phalcon PHP framework
GD enabled on the PHP target Host


### How do I get set up? ###

* Configuration

Create mysql database from provided dump file:
sql/127.0.0.1.arcanum.6-11-14.10-28 AM.sql

The phalcon PHP framework, mysql and GD need to be enabled on the PHP server.

In photos/app/config/config.php, the BASE_PATH, BASE_URL and database settings need to be updated for your environment.

In photos/public, the .htaccess file *may* need a RewriteBase set. For example, if your serving out of ~/<usernmame>, like I do for local development.

Example:

RewriteBase /~cwt/photos


In angular_see/app/js/apps.js, the var base_url will need to be set for your environment.

var base_url = 'http://127.0.0.1/~cwt/photos/photo';

The photos/public/media directory needs to be writable by the webserver:

chmod -R 777 photos/public/media

For convenience in my local setup, I have cloned the code and created symlinks into my webservers DOCROOT:

ln -s /Users/cwt/Code/arcanum/photos/public/ photos
ln -s /Users/cwt/Code/arcanum/angular-seed/app/ arc

This allows me to browse the angular app like so:

http://127.0.0.1/~cwt/arc/